/*
 * HoverController.h
 *
 *  Created on: 19/06/2012
 *      Author: Ignacio Mellado-Bataller
 */

#ifndef HOVERCONTROLLER_H_
#define HOVERCONTROLLER_H_

#include "control/PID.h"
#include <string>
#include <iostream>
#include "xmlfilereader.h"

namespace CVG {
namespace MAV {

class HoverController {

public:
    HoverController(int idDrone, const std::string &stackPath_in);
    void run(double Vx_measure, double Vy_measure, double altitude, float *pitchCmdDeg, float *rollCmdDeg);
    void enable(bool e);
    inline bool isEnabled() { return enabled; }

public:
    void init(std::string configFile);
    bool readConfigs(std::string configFile);

private:
    CVG_BlockDiagram::PID pidVx, pidVy;
    bool enabled;

    // Configuration parameters
    double GAIN_P, GAIN_I, GAIN_D;
    double MAX_OUTPUT;
    double ALTITUDE_FOR_NOMINAL_D;
};

}
}

#endif /* HOVERCONTROLLER_H_ */
